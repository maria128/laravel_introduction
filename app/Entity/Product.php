<?php

declare(strict_types=1);

namespace App\Entity;

class Product
{

    private $id;
    private $name;
    private $price;
    private $imageUrl;
    private $rating;
    public function __construct
    (
        $id,
        $name,
        $price,
        $imageUrl,
        $rating

    )
    {
        $this->name = $name;
        $this->id = $id;
        $this->price = $price;
        $this->imageUrl = $imageUrl;
        $this->rating = $rating;

    }


    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    public function getRating()
    {
        return $this->rating;
    }

}