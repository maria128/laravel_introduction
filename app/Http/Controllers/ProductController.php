<?php

namespace App\Http\Controllers;

use App\Action\Product\GetAllProductsAction;
use App\Action\Product\GetCheapestProductsAction;
use App\Action\Product\GetMostPopularProductAction;
use App\Http\Presenter\ProductArrayPresenter;
use App\Http\Response\ApiResponse;

class ProductController extends ApiController
{

    private $presenter;
    private $getAllProducts;
    private $getMostPopularProduct;
    private $getCheapestProducts;

    public function __construct
    (
        ProductArrayPresenter $presenter,
        GetAllProductsAction $getAllProducts,
        GetMostPopularProductAction $getMostPopularProduct,
        GetCheapestProductsAction $getCheapestProducts

    )
    {
        $this->presenter = $presenter;
        $this->getAllProducts = $getAllProducts;
        $this->getMostPopularProduct = $getMostPopularProduct;
        $this->getCheapestProducts = $getCheapestProducts;
    }

    public function getProductCollection(): ApiResponse
    {
        $response = $this->getAllProducts->execute()->getProducts();

        return $this->createSuccessResponse($this->presenter::presentCollection($response));
    }

    public function getMostPopularProduct(): ApiResponse
    {
        $response = $this->getMostPopularProduct->execute()->getProduct();

        return $this->createSuccessResponse($this->presenter::present($response));
    }


    public function getCheapestProducts()
    {
        $response = $this->getCheapestProducts->execute()->getProducts();

        return view('cheap_products', ['products' => $response]);

    }
}