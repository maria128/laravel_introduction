<?php

namespace App\Providers;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Repository\ProductRepositoryInterface;
use App\Services\ProductGenerator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    private $products;
    /**
     * Register any application services.
     *
     * @return void
     */

    public function register()
    {
        $products =  ProductGenerator::generate();
        $this->app->bind('App\Repository\ProductRepositoryInterface', function () use ($products) {

            return new ProductRepository($products);

        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
