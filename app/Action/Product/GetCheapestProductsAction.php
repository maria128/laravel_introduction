<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetCheapestProductsAction
{
    private $products;

    public function __construct(ProductRepositoryInterface $products)
    {
        $this->products = $products;

    }
    
    public function execute(): GetCheapestProductsResponse
    {
        return new GetCheapestProductsResponse($this->products->findCheapestProduct());
    }
}