<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product;

class ProductRepository implements ProductRepositoryInterface
{
	private  $products = [];
    /**
     * @param Product[] $products
     */
    public function __construct(array $products)
    {
        $this->products = $products;
    }

    /**
     * @return Product[]
     */
    public function findAll(): array
    {
        return $this->products;
    }


    public function findMostPopularProduct(): Product
    {
        return max(array_map('self::callbackMostPopularProduct', $this->products));

    }

    public function findCheapestProduct(): array
    {
        $arrayProducts = $this->products;
        usort($arrayProducts, "self::callbackUsortPriceCheapestProduct");
        $arrayProducts = array_slice($arrayProducts, 0, 3);
        return $arrayProducts;
    }

    private  function callbackMostPopularProduct($product)
    {
        return $product;
    }

    private function callbackUsortPriceCheapestProduct(Product $a, Product $b)
    {

        if ($a->getPrice() == $b->getPrice()) {
            return 0;
        }
        return ($a->getPrice() > $b->getPrice()) ? +1 : -1;
    }
}
