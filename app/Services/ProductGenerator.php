<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Product;

class ProductGenerator
{
    /**
     * @return Product[]
     */
    public static function generate(): array
    {
        $users = [];

        for($i = 1; $i < 5; $i++) {

            $id =  $i;
            $name = 'product' . $i;
            $price = 100.11 * $i;
            $url = 'url'. $i;
            $rating = 1.1 * $i;

            $users[] = new  Product
            (
                $id,
                $name,
                $price,
                $url,
                $rating

            );
        }

        return $users;
    }
}