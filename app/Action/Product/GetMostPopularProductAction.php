<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetMostPopularProductAction
{
    private $products;

    public function __construct(ProductRepositoryInterface $products)
    {
        $this->products = $products;

    }

    public function execute(): GetMostPopularProductResponse
    {
        return  new GetMostPopularProductResponse($this->products->findMostPopularProduct());
    }
}