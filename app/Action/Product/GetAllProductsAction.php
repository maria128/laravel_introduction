<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetAllProductsAction
{
    private $products;

    public function __construct(ProductRepositoryInterface $products)
    {
        $this->products = $products;

    }

    public function execute(): GetAllProductsResponse
    {

        return  new GetAllProductsResponse($this->products->findAll());
    }
}